from django.urls import re_path, include
from .views import index, register
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path('register', register, name='register'),
]
