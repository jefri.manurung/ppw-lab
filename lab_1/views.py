from django.shortcuts import render, redirect
from datetime import datetime, date
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login

# Enter your name here
mhs_name = 'JEFRI MANURUNG' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000,2,23) #TODO Implement this, format (Year, Month, Date)
npm = None # TODO Implement this
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')
    else:
        form = UserCreationForm()

    context = {'form' : form}
    return render(request, 'registration/register.html', context)
