from django.shortcuts import render
from lab_1.views import mhs_name, birth_date
# Create your views here.
landing_page_content = ''

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'skills.html', response)
