$(document).ready(function()
{
  var tema1=[
    "Black", "White"
  ];

  var tema = [
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":1,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":2,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":3,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"} ];

  var selectedTheme={"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

  $("#tema").select2({
    data: tema
  });

  $("button").click(function(){
    alert("Maaf, fungsi belum bisa berjalan :)");
  });

  var input = document.getElementById("chat");
  input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      document.getElementById("myText").value = "Masih Belajar"
    }
  });
});
