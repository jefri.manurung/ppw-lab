"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import index as index_lab1, register


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
    re_path(r'^my_experience/', include('my_experience.urls')),
    re_path(r'^education/', include('education.urls')),
    re_path(r'^skills/', include('skills.urls')),
    re_path(r'^experience/', include('experience.urls')),
    re_path(r'^project/', include('project.urls')),
    re_path(r'^guestbook/', include('guestbook.urls')),
    re_path(r'^chatbox/', include('chatbox.urls')),
    re_path('accounts/', include('django.contrib.auth.urls')),
    re_path(r'^$', index_lab1, name='index'),
    re_path('register', register, name='register')
]
