from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

def index(request):
    response = {}
    response['message_form'] = Message_Form()
    # response = {'message_form': Message_Form}
    return render(request, 'guestbook.html', response)

def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response = {}
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'], message=response['message'])
        message.save()
        html = 'form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/guestbook/')

def message_table(request):
    response = {}
    message = Message.objects.all()
    response['message'] = message
    html = 'table.html'
    return render(request, html , response)
