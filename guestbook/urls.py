from django.urls import path
from .views import index, message_post, message_table
from django.urls import re_path
from django.conf.urls import url

#url for app, add your URL Configuration

app_name = "guestbook"

urlpatterns = [
#TODO Implement this
    re_path(r'^$', index, name='index'),
    re_path(r'^add_message', message_post, name='add_message'),
    re_path(r'^result_table', message_table, name='result_table'),
    # url(r'^guestbook/', views.guestbook, name='guestbook'),
]
